/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package perfecto;

import java.util.Scanner;

/** 
 *
 * @author zabek
 */
public class PerfectoChecker {

    /** Generates random numbers and check how many of them are perfect
     *
     * @param args unused
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of numbers to test (for n > 100000 it can be very slow)");
        int count = scanner.nextInt();
        while(count < 1){
            System.out.println("Number must be bigger than 0");
            count = scanner.nextInt();
        }
        
        int perfectCount = 0;
        for (int i = 0; i < count; i++) {
            if(PerfectoChecker.checkIfPerfect(PerfectoChecker.randomNumber())){
                perfectCount ++;
            }
        }
        
        System.out.println("Percent of perfect numbers is  " + (perfectCount / (double)count * 100.0d) + "%");
    }

    /** Check if a number is perfect
     *
     * @param number number to check
     * @return true if perfect, otherwise false
     */
    public static boolean checkIfPerfect(int number) {
        if (number < 1) {
            return false;
        }

        int sum = 0;
        for (int i = 1; i <= number / 2; i++) {
            if (number % i == 0) {
                sum += i;
            }
        }

        return sum == number;
    }

    
    private static int randomNumber() {
        int rand = (int) ((System.nanoTime() / 101.3d % (Math.PI * 3123.210d)) * (new Object().hashCode() / 3411.2d %223213.3d));
        return (int) ( (System.nanoTime() / 100L % 10000L  * rand / 10) % 101230);
    }

}

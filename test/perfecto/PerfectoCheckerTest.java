/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package perfecto;

import org.junit.Test;
import static org.junit.Assert.*;
/**
 *
 * @author zabek
 */
public class PerfectoCheckerTest {
    
    @Test
    public void shouldBePerfect(){
        assertTrue(PerfectoChecker.checkIfPerfect(6));
        assertTrue(PerfectoChecker.checkIfPerfect(28));
        assertTrue(PerfectoChecker.checkIfPerfect(496));
        assertTrue(PerfectoChecker.checkIfPerfect(8128));
    }
    
    @Test
    public void shouldNotBePerfect(){
        assertFalse(PerfectoChecker.checkIfPerfect(27));
        assertFalse(PerfectoChecker.checkIfPerfect(251));
        assertFalse(PerfectoChecker.checkIfPerfect(55432));
    }
}
